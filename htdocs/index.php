<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheetreset" href="css/reset.css" />
    <link rel="script" href="scripts/process.php">
    <title>Assignment 04 -- Egri-Blogna, Jayden</title>
</head>

<body>
    <section>
        <img src="central-park.jpg" alt="Centeral-Park">
        <h2>PHOTO DETAILS</h2>

        <form method="get" action="scripts/process.php">
          <fieldset>
             <table>
                 <tr>
                    <td colspan="2">
                        <label>Title</label><br>
                        <input type="text" name="title" size="87" placeholder="Give your photo a descriptive name">
                        <br><br>
                        <label>Description</label><br>
                        <textarea name="description" rows="4" cols="75" placeholder="Adding a rich description will help search results"></textarea>
                    </td>
                 </tr>
                 <tr>
                    <td>
                        <label>Continent </label>
                        <br>
                        <select name="continent">
                           <option>Choose continent </option>
                           <option>Africa</option>
                           <option>Asia</option>
                           <option>Australia</option>
                           <option>Europe</option>
                           <option>North America</option>
                           <option>South America</option>
                        </select>
                        <br>
                        <br>
                        <label>Country </label>
                        <br>
                        <select name="country">
  		                     <option>Choose country </option>
  		                     <option>Albania</option>
  		                     <option>Belgium</option>
  		                     <option>Denmark</option>
  		                     <option>Italy</option>
  		                     <option>Sweden</option>
                        </select>
                        <br>
                        <br>
                        <label>City </label>
                        <br>
                        <input type="text" name="city" />
                      </td>
                      <td>
                          <div class="bordering">
                             <label>Copyright?<br /></label>
                             <input type="radio" name="copyright" value="1"/>All rights reserved<br>
                             <input type="radio" name="copyright" value="2" checked/>Creative Commons<br>
                          </div>

                          <div class="bordering">
                             <label>Creative Commons Types<br /></label>
                             <input type="checkbox" name="attribution" checked/>Attribution<br>
                             <input type="checkbox" name="noncommercial"/>Noncommercial<br>
                             <input type="checkbox" name="derivative"/>No Derivative Works<br>
                          </div>
                      </td>
                    </tr>
                    <tr class="backcolor">
                      <td colspan="2">
                         <label>I accept the software license</label>
                         <input type="checkbox" name="accept" />
                         <br>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <label>Rate this photo: </label><br>
                        <input type="number" min="1" max="5" name="rate"/>
                        <br>
                        <br>
                        <label>Color Collection: <br />
                        <input type="color" name="back" />
                      </td>
                      <td>
                          <div class="bordering">
                             <label>Date Taken: </label>
                             <br>
                             <input type="date"/>
                             <br>
                             <br>
                             <label>Time Taken: </label>
                             <br>
                             <input type="time"/>
                          </div>
                      </td>
                    </tr>
                    <tr class="backcolor">
                      <td colspan="2">
                         <button type="submit" value="Submit" id="button">Submit</button>
                         <button type="reset" value="Clear" id="button">Clear Form</button>
                      </td>
                    </tr>
             </table>
          </fieldset>
        </form>
    </section>
</body>
</html>
